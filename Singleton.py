class Singleton(object):
    __instance = None

    class __Singleton:
        def __init__(self):
            self.val = "some"

    def __new__(cls):
        if not Singleton.__instance:
            Singleton.__instance = Singleton.__Singleton()
        return Singleton.__instance


a = Singleton()
b = Singleton()
print(a)
print(b)
